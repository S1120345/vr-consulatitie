﻿using System.Collections;
using System.IO;
using NUnit.Framework;
using UnityEngine.TestTools;

public class FileExplorerTests
{
    [TearDown]
    public void TearDown()
    {
        var di = new DirectoryInfo("V3R/Test");
        foreach (FileInfo file in di.EnumerateFiles())
        {
            file.Delete(); 
        }
        foreach (DirectoryInfo dir in di.EnumerateDirectories())
        {
            dir.Delete(true); 
        }
    }
    [Test]
    public void CreateFileExplorerTest()
    {
        var fileExplorer = new FileExplorer("V3R/Test");
        Assert.IsNotNull(fileExplorer);
    }
    
    [Test]
    public void CreateDirectoryTest()
    {
        var fileExplorer = new FileExplorer("V3R/Test");
        fileExplorer.CreateFolder("testfolder");
        Assert.IsTrue(Directory.Exists("V3R/Test/testfolder"));
    }
    
    [Test]
    public void CreateFileTest()
    {
        var fileExplorer = new FileExplorer("V3R/Test");
        fileExplorer.CreateFile("testfile.dat");
        Assert.IsTrue(File.Exists("V3R/Test/testfile.dat"));
    }

    public void GoToFolderTest()
    {
        
    }
}