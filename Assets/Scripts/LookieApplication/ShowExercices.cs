﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class ShowExercices : MonoBehaviour
{
    #region Serialized Fields

    public Material material;

    #endregion

    private int _currentIndex;

    private List<Texture2D> _texture2Ds;

    #region Event Functions

    // Start is called before the first frame update
    private void Start()
    {
        _texture2Ds = new List<Texture2D>();
        GetTextures();
        ApplyTexture();
        StartCoroutine(GetTexture());
    }

    // Update is called once per frame
    private void Update()
    {
    }

    #endregion

    private void OnNextExercise()
    {
        _currentIndex += 1;
        if (_currentIndex > _texture2Ds.Count - 1)
            _currentIndex = 0;
        ApplyTexture();
    }

    private void OnPrevExercise()
    {
        _currentIndex -= 1;
        if (_currentIndex < 0)
            _currentIndex = _texture2Ds.Count - 1;
        ApplyTexture();
    }


    private IEnumerator GetTexture()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture("https://picsum.photos/1920/1080");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
            Debug.Log(www.error);
        else
            _texture2Ds.Add(((DownloadHandlerTexture) www.downloadHandler).texture);
    }

    private void GetTextures()
    {
        if (Directory.Exists(Application.streamingAssetsPath          + "/Exercises") == false)
            Directory.CreateDirectory(Application.streamingAssetsPath + "/Exercises");
        string[] files = Directory.GetFiles(Application.streamingAssetsPath + "/Exercises");
        foreach (string file in files)
            if (file.EndsWith(".png"))
            {
                byte[] rawData = File.ReadAllBytes(file);
                Texture2D tex = new Texture2D(2, 2);
                tex.LoadImage(rawData);
                _texture2Ds.Add(tex);
            }

        ApplyTexture();
    }

    private void ApplyTexture()
    {
        material.mainTexture = _texture2Ds[_currentIndex];
    }
}