﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using V3R.Multiplayer;

public class Chat : MonoBehaviour
{
    #region Serialized Fields

    public TextMeshProUGUI text;
    public GameObject ui;
    public TMP_InputField inputField;
    public Settings Settings;

    #endregion

    [NonSerialized] public bool typing;
    private List<string> _chat;
    private ChatChannel[] _chatChannels;
    private Dictionary<string, int> _updateCheck;
    private bool SetupDone = false;
    
    #region Event Functions

    // Start is called before the first frame update
    private void Start()
    {
        _updateCheck = new Dictionary<string, int>();
        _chat = new List<string>();
    }
    


    // Update is called once per frame
    private void Update()
    {
        if (SetupDone == false)
            if (!GameObject.Find("Multiplayer").GetComponent<MultiplayerSetup>().run)
            {
                _chatChannels = GameObject.Find("Multiplayer").GetComponents<ChatChannel>();
                SetupDone = true;
                Debug.Log(_chatChannels.Length);
            }
        if (SetupDone)
            Updatechat();
    }

    #endregion

    /// <summary>
    ///     Send the input from the textfield
    /// </summary>
    public void SendChatMessage()
    {
        _chatChannels[0].SendMessage(inputField.text);
        inputField.text = "";
    }

    /// <summary>
    ///     gets called when the UI control is being pressed
    /// </summary>
    private void OnChat()
    {
        if (typing)
            SendChatMessage();
        else
            ToggleUI();
    }

    /// <summary>
    ///     enable/disable the UI and movement of the player
    /// </summary>
    private void ToggleUI()
    {
        // check if this script is disabled
        if (enabled == false)
            return;
        // toggle the UI
        ui.SetActive(!ui.activeInHierarchy);
    }

    private void UpdateText()
    {
        text.text = "";
        text.text = string.Join("\n", _chat);
    }

    public void Textfieldfocus(bool focus)
    {
        Debug.Log("triggered");
        typing = focus;
    }

    private void Updatechat()
    {
        // todo order chat on time 
        // go trough every channel
        foreach (ChatChannel channel in _chatChannels)
        {
            //get messages in the channel
            List<Dictionary<string, string>> messages = channel.ReturnMessages();
            // check/add channel to to update list
            if (_updateCheck.ContainsKey(channel.name) == false)
                _updateCheck.Add(channel.name, 0);
            // if there isn't a update skip channel
            if (messages.Count == _updateCheck[channel.name])
                continue;
            // update the channel list
            _updateCheck[channel.name] = messages.Count;
            _chat.Clear();
            // loop trough the messages
            foreach (Dictionary<string, string> returnMessage in messages)
            {
                string messageChannel = null;
                string messageText = null;
                string messageSender = null;
                // loop trough the message to access all info
                foreach (KeyValuePair<string, string> messagePair in returnMessage)
                    switch (messagePair.Key)
                    {
                        case "content":
                            string[] split = messagePair.Value.Trim('{', '}').Split(':');
                            messageChannel = split[0].Trim('"');
                            messageText = split[1].Trim('"');
                            break;
                        case "senderName":
                            messageSender = messagePair.Value;
                            break;
                    }

                // add text to chat
                _chat.Add($"{messageSender}@{messageChannel}: {messageText}");
            }

            UpdateText();
        }
    }
}