﻿using UnityEngine;
using UnityEngine.UI;

public class CameraScript : MonoBehaviour
{
    private static WebCamTexture _webCam;

    #region Event Functions

    // Start is called before the first frame update
    private void Start()
    {
        if (_webCam == null)
            _webCam = new WebCamTexture();
        GetComponent<RawImage>().texture = _webCam;

        if (_webCam.isPlaying == false)
            _webCam.Play();
    }

    private void OnEnable()
    {
        if (_webCam.isPlaying == false)
            _webCam.Play();
    }

    private void OnDisable()
    {
        if (_webCam.isPlaying)
            _webCam.Stop();
    }

    #endregion
}