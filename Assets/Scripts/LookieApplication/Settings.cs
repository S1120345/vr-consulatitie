﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Settings : MonoBehaviour
{
    #region Serialized Fields

    public GameObject ui;
    public Chat chat;
    public GameObject mirror;
    public GameObject webCam;
    public GameObject microphone;
    #endregion

    public void ToggleUI()
    {
        if (chat.typing) return;
        ui.SetActive(!ui.activeInHierarchy);
        chat.enabled = !ui.activeInHierarchy;
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    public void Mirror(bool input)
    {
        mirror.SetActive(input);
    }

    public void Webcam(bool input)
    {
        webCam.SetActive(input);
    }

    public void Microphone(bool input)
    {
        // todo add microphone script
        Debug.Log($"Microfoon is {input}");
        // microphone.SetActive(input);
    }

    private void OnSettings()
    {
        ToggleUI();
    }
}