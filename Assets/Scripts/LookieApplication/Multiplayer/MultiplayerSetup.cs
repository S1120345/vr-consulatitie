﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V3R.Connect.Multiplayer;
using V3R.Multiplayer;
using V3R.Utility.Recording;

public class MultiplayerSetup : MonoBehaviour
{
    [SerializeField] public Lookie Lookie;
    [SerializeField] public AvatarDatabase avatarDatabase;
    [SerializeField] public GameObject session;
    public Ghosthands ghosthands;
    public Ghosthands remoteghosthands;
    public PlayBackEngine playBackEngine;
    public GameObject mirror;
    [NonSerialized]  public bool run = true;


    public bool webgl;
    public string matchid;

    public void Update()
    {
        if (run)
            if (session.GetComponent<SessionWithPreferences>().done)
            {
                Setup();
                session.GetComponent<SessionWithPreferences>().done = false;
                run = false;
            }
    }

    // Start is called before the first frame update
    private void Setup()
    {
        string id;
        if (webgl)
        {
            URLParams @params = new URLParams();
            var paramsURL= @params.GETParamsURL();
            id = paramsURL["id"];
            Debug.Log(id);
        }
        else
        {
            id = matchid;
        }
            
        // add MatchSys
        var matchSys = gameObject.AddComponent<MatchSys>();
        matchSys.matchId = id;
        matchSys.debugAutoCreate = false;
        matchSys.debugAutoJoin = false;
        matchSys.joinExistingMatch = true;
        // add ChatChannel
        var chat = gameObject.AddComponent<ChatChannel>();
        chat.channelName = id;
        // add PlayerPresenceHandler
        var playerPresence = gameObject.AddComponent<PlayerPresenceHandler>();
        playerPresence._multiplayer = matchSys;
        playerPresence._avatarDatabase = avatarDatabase;
        // add PlayerPresenceLookieHandler
        var playerLookieHandeler = gameObject.AddComponent<PlayerPresenceLookieHandler>();
        playerLookieHandeler.multiplayer = matchSys;
        playerLookieHandeler.Lookie = Lookie;
        playerLookieHandeler.playerPresenceHandler = playerPresence;
        // add PlayerPresenceGhostHandsHandler
        var ghosthandsHandler = gameObject.AddComponent<PlayerPresenceGhostHandsHandler>();
        ghosthandsHandler.multiplayer = matchSys;
        ghosthandsHandler.ghosthands = ghosthands;
        ghosthandsHandler.Remoteghosthands = remoteghosthands;
        ghosthandsHandler.playerPresenceHandler = playerPresence;
        ghosthandsHandler.PlayBackEngine = playBackEngine;
        // add PlayerPresenceMirrorHandler
        var mirrorHandler = gameObject.AddComponent<PlayerPresenceMirrorHandler>();
        mirrorHandler.multiplayer = matchSys;
        mirrorHandler.playerPresenceHandler = playerPresence;
        mirrorHandler.Mirror = mirror;
        
        var playerPresenceArmature = gameObject.AddComponent<PlayerPresenceArmatureHandler>();
        playerPresenceArmature._multiplayer = matchSys;
        playerPresenceArmature._playerPresenceHandler = playerPresence;
    }
}
