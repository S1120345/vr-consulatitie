﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V3R.Common;
using V3R.Multiplayer;

public class DestoryMatch : MonoBehaviour
{
    public MultiplayerSetup MultiplayerSetup;
    private MatchSys _matchSys;
    private bool _run = true;
    // Update is called once per frame
    void Update()
    {
        if (_run)
            if (MultiplayerSetup.run == false)
            {
                _matchSys = gameObject.FindComponent<MatchSys>();
                _run = false;
            }   
    }

    private void OnDestroy()
    {
        _matchSys.SendMatchState(0, JsonUtility.ToJson(null) );
    }
}