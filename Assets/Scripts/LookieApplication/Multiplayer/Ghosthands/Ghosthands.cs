﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghosthands : MonoBehaviour
{
    public List<Transform> Transforms => getTransforms(transform);
    public bool active => gameObject.activeInHierarchy;

    private List<Transform> getTransforms(Transform baseTransform)
    {
        List<Transform> transforms = new List<Transform>();

        void RecursiveFill(Transform parent)
        {
            transforms.Add(parent);
            foreach (Transform child in parent.transform)
                RecursiveFill(child);
        }

        RecursiveFill(baseTransform);
        return transforms;
    }
}
