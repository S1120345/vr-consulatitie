﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class URLParams 
{
    private Dictionary<string,string> _params;
    private string _url;

    public URLParams()
    {
        _url = Application.absoluteURL;
        _params = new Dictionary<string, string>();
        foreach (string paramString in _url.Split('?')[1].Split('&'))
        {
            var param = paramString.Split('=');
            _params.Add(param[0], param[1]);
        }
    }

    public Dictionary<string,string> GETParamsURL()
    {
        return _params;
    }
}
