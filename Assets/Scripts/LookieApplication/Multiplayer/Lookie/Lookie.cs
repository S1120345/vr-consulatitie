﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using V3R.Common;

public class Lookie : MonoBehaviour
{
    public Transform[] transforms => new Transform[] {_lookie,_lookieHead};
    private Transform _lookie => transform; 
    private Transform _lookieHead => _lookie.FindChildDepth("Head bone").transform;
}
