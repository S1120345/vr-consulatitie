﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using Nakama.TinyJson;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Networking;
using UnityEngine.UI;
using V3R.Utility;
using V3R.Utility.Recording;
using Debug = UnityEngine.Debug;
using Space = V3R.Utility.Space;

namespace V3R.Examples
{
    public class RecordingSceneController : MonoBehaviour
    {
        #region Variables

        #region Serialized Fields

        [Header("Player & Ghost Player")] [SerializeField]
        private Transform _playerTransform;

        [SerializeField] private Transform _ghostPlayerTransform;

        [Header("UI Elements")] [SerializeField]
        private Slider _frameSlider;

        [SerializeField] private Button _saveButton;
        [SerializeField] private Button _loadButton;
        [SerializeField] private Button _recordButton;
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _stopButton;

        #endregion

        private const string RECORDING_PATH = "V3R/DEMO/RecordingExample.dat";

        private readonly RecorderSettings _recorderSettings = new RecorderSettings();

        private PlayBackEngine _playbackEngine;
        private Recorder _recorder;

        private RecordingData _recording;
        private bool _shouldRecord;

        private List<Transform> _playerTransformList;
        private List<Transform> _ghostPlayerTransformList;

        #endregion

        #region Functions

        #region Event Functions

        private void Start()
        {
            _playbackEngine = _ghostPlayerTransform.GetComponent<PlayBackEngine>();

            _recorderSettings.PositionSpace = Space.WORLD;
            _recorderSettings.RotationSpace = Space.WORLD;
            _playerTransformList = getTransforms(_playerTransform);
            _ghostPlayerTransformList = getTransforms(_ghostPlayerTransform);
        }

        private void FixedUpdate()
        {
            if (_shouldRecord)
                _recorder.Update();
        }

        #endregion

        public void ToggleRecording()
        {
            if (_shouldRecord)
                StopRecording();
            else
                StartRecording();
        }

        public void PlayRecording()
        {
            Debug.Log("play");
            _ghostPlayerTransform.gameObject.SetActive(true);
            _playbackEngine.SetRecordingData(_recording, getTransforms(_ghostPlayerTransform).ToArray());
            _playbackEngine.StartRecording();
            UpdateRecordingButton();
        }

        public void StopPlayRecording()
        {
            Debug.Log("stop");
            _ghostPlayerTransform.gameObject.SetActive(false);
            _playbackEngine.StopRecording();
            _playbackEngine.timer.Close();
            UpdateRecordingButton();
        }

        [DllImport("__Internal")]
        public static extern void BrowserDownload(string filename, string textContent);
        
        public void save()
        {
            var binaryFormatter = new BinaryFormatter();
            MemoryStream ms = new MemoryStream();
            binaryFormatter.Serialize(ms, _recording);
            ms.Seek(0, SeekOrigin.Begin);
            var test= Convert.ToBase64String(ms.ToArray());  
            BrowserDownload("test.dat", test);
        }
        
        [DllImport("__Internal")]
        public static extern void UploadFile();
        
        public void loadButtonPressed()
        {
            UploadFile();
        }
        
        public IEnumerator load(string path)
        {
            var www = UnityWebRequest.Get(path);
            yield return www.SendWebRequest();
            
            if (www.isNetworkError || www.isHttpError)
                Debug.Log(www.error);
            else
            {
                var file = www.downloadHandler.data;
                var binaryFormatter = new BinaryFormatter();
                Debug.Log(file.Length);
                MemoryStream ms = new MemoryStream();
                ms.Write(file, 0,file.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var obj = binaryFormatter.Deserialize(ms);
                _recording = (RecordingData) obj;
            }
                
            UpdateRecordingButton();
        }

        public void SetRecordingFrame(float index)
        {
            _playbackEngine.SetRecordingIndex((int) index);
            UpdateRecordingButton();
        }

        private List<Transform> getTransforms(Transform baseTransform)
        {
            List<Transform> _transforms = new List<Transform>();

            void RecursiveFill(Transform parent)
            {
                _transforms.Add(parent);
                foreach (Transform child in parent.transform)
                    RecursiveFill(child);
            }
            
            RecursiveFill(baseTransform);
            Debug.Log(_transforms.Count);
            return _transforms;
        }

        private void StartRecording()
        {
            if (_playbackEngine.IsPlaying() == false)
                _recorder = new Recorder(_playerTransformList.ToArray(), _recorderSettings);
            _shouldRecord = true;
            UpdateRecordingButton();
        }

        private void StopRecording()
        {
            if (_shouldRecord)
            {
                _shouldRecord = false;

                RecordingData recordingData = _recorder.RecordingData;
                Debug.Log(recordingData.Data.Count);
                _recording = recordingData;
                UpdateRecordingButton();
            }
        }

        private void UpdateRecordingButton()
        {
            if (_shouldRecord)
            {
                _saveButton.interactable = false;
                _playButton.interactable = false;
                _stopButton.interactable = false;
                _loadButton.interactable = false;
                _frameSlider.interactable = false;
            }
            else
            {
                _loadButton.interactable = true;
                if (_recording != null)
                {
                    _saveButton.interactable = true;
                    _playButton.interactable = true;
                    _stopButton.interactable = _playbackEngine.IsPlaying();
                    _frameSlider.interactable = true;
                    _frameSlider.maxValue = _recording.Data.Count - 1;
                }
            }
        }

        #endregion
    }
}