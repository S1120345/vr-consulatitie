﻿using System.IO;

public class FileExplorer
{
    private DirectoryInfo _currentPath;

    public FileExplorer(string path)
    {
        _currentPath = new DirectoryInfo(path);
    }

    public string CurrentPath => _currentPath.FullName;

    public DirectoryInfo[] GETFolders()
    {
        return _currentPath.GetDirectories();
    }

    public FileInfo[] GETFiles()
    {
        return _currentPath.GetFiles();
    }

    public void GoToParentDirectory()
    {
        _currentPath = _currentPath.Parent;
        if (_currentPath == null) _currentPath = new DirectoryInfo("/");
    }

    public string[] GETDrives()
    {
        return Directory.GetLogicalDrives();
    }

    public void GoToFolder(DirectoryInfo folder)
    {
        _currentPath = folder;
    }

    public void Rename(FileInfo file, string newName)
    {
        file.MoveTo(file.DirectoryName + "/" + newName + file.Extension);
    }

    public void Rename(DirectoryInfo folder, string newName)
    {
        if (folder.Parent != null) 
            folder.MoveTo(folder.Parent.Name + "/" + newName);
    }

    public void CreateFolder(string folderName)
    {
        Directory.CreateDirectory(_currentPath + "/" + folderName);
    }
    
    public void CreateFile(string fileName)
    {
        var temp = File.Create(_currentPath + "/" + fileName);
        temp.Close();
    }
}