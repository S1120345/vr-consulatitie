﻿using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FileExplorerUI : MonoBehaviour
{
    #region Variables

    #region Serialized Fields

    [SerializeField] private GameObject templateFolder;

    [SerializeField] private GameObject templateFile;

    #endregion

    private List<GameObject> _currentObjects;

    private FileExplorer _fileExplorer;

    #endregion

    #region Functions

    #region Event Functions

    // Start is called before the first frame update
    private void Start()
    {
        _fileExplorer = new FileExplorer("V3R/DEMO");
        _currentObjects = new List<GameObject>();
        UpdateExplorer();
    }

    #endregion

    public void GoToParrentFolder()
    {
        _fileExplorer.GoToParentDirectory();
        UpdateExplorer();
    }

    public void GoToFolder(DirectoryInfo folder)
    {
        _fileExplorer.GoToFolder(folder);
        UpdateExplorer();
    }

    public void FileSelected(FileInfo file)
    {
        Debug.Log(file.Name);
    }

    private void UpdateExplorer()
    {
        foreach (var gameObject in _currentObjects) Destroy(gameObject);
        var folders = _fileExplorer.GETFolders();
        foreach (var folder in folders) createFolder(folder);
        var files = _fileExplorer.GETFiles();
        foreach (var file in files) createFile(file);
    }


    private void createFile(FileInfo file)
    {
        var fileObject = Instantiate(templateFile, templateFile.transform.parent);
        _currentObjects.Add(fileObject);
        fileObject.name = file.Name;
        fileObject.GetComponentInChildren<TextMeshProUGUI>().text = file.Name;
        fileObject.GetComponent<Button>().onClick.AddListener(() => FileSelected(file));
        fileObject.SetActive(true);
    }

    private void createFolder(DirectoryInfo folder)
    {
        var folderObject = Instantiate(templateFolder, templateFolder.transform.parent);
        _currentObjects.Add(folderObject);
        folderObject.name = folder.Name;
        folderObject.GetComponentInChildren<TextMeshProUGUI>().text = folder.Name;
        folderObject.GetComponent<Button>().onClick.AddListener(() => GoToFolder(folder));
        folderObject.SetActive(true);
    }

    #endregion
}