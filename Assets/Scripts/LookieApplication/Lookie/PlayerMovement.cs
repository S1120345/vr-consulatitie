﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    #region Serialized Fields

    public CharacterController playerCharacterController;
    public float speed;
    public Transform camera;
    public bool mouseInverted;
    public float mouseSensitivity = 15;
    public Transform headbone;
    public List<GameObject> UIElements;

    #endregion

    private float _heigthMovement;
    private Vector2 _mouseVector2 = new Vector2(0, 0);
    private Vector2 _moveVector = new Vector2(0, 0);
    private float _xRotation;

    #region Event Functions

    private void Start()
    {
        playerCharacterController = GetComponent<CharacterController>();
    }

    public void Update()
    {
        if (CheckUIElementsNotActive() == false)
        {
            CursorEnabled();
            return;
        }
            
        CursorDisabled();
        Vector3 finalVector = transform.right * _moveVector.x + transform.forward * _moveVector.y +
                              transform.up    * _heigthMovement;
        

        // configure the mouse x,y
        float _mouseX = _mouseVector2.x * mouseSensitivity * Time.deltaTime;
        float mouseY = _mouseVector2.y  * mouseSensitivity * Time.deltaTime;
        // invert the mouse
        if (mouseInverted == false)
            _xRotation -= mouseY;
        else
            _xRotation += mouseY;
        // Clamp the mouse y value 
        _xRotation = Mathf.Clamp(_xRotation, -90f, 90f);
        camera.localRotation = Quaternion.Euler(_xRotation, 0, 0);
        headbone.localRotation = Quaternion.Euler(_xRotation, 0, 0);
        transform.Rotate(Vector3.up * _mouseX);
        playerCharacterController.Move(finalVector * (Time.deltaTime * speed));
    }

    private bool CheckUIElementsNotActive()
    {
        return UIElements.All(uiElement => !uiElement.activeSelf);
    }

    private static void CursorDisabled()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void CursorEnabled()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
    }

    #endregion

    public void OnMove(InputValue context)
    {
        _moveVector = context.Get<Vector2>();
    }

    public void OnHeight(InputValue context)
    {
        _heigthMovement = context.Get<float>();
    }

    public void OnLook(InputValue context)
    {
        // Read the mouse value
        _mouseVector2 = context.Get<Vector2>();
    }

    public void SetMouseseSensitivity(float input)
    {
        mouseSensitivity = input;
    }
}