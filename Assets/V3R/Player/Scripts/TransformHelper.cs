﻿// Copyright (c) 2021 V3RLabs
// Created By: Jelle van Ommen

using System.Collections.Generic;
using UnityEngine;

namespace V3R.MirrorRoom.Player
{
    public static class TransformHelper
    {
        public static void GetAllChildren(ICollection<Transform> list, Transform sourceTransform)
        {
            Transform[] children = sourceTransform.GetComponentsInChildren<Transform>();

            foreach (Transform child in children)
                list.Add(child);
        }


        public static void GetAllChildrenDepth(this Transform sourceTransform, ICollection<Transform> list)
        {
            var stack = new Stack<Transform>();
            stack.Push(sourceTransform);

            while (stack.Count > 0)
            {
                Transform current = stack.Pop();

                list.Add(current);

                for (int i = current.childCount - 1; i >= 0; i--)
                    stack.Push(current.GetChild(i));
            }
        }
    }
}
