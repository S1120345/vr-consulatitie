﻿// Copyright (c) 2021 V3RLabs
// Created By: Jelle van Ommen

using UnityEngine;

namespace V3R.MirrorRoom.Player
{
    public enum MirrorDirection { NONE, LEFT_TO_RIGHT, RIGHT_TO_LEFT }

    public class MirrorModel : MonoBehaviour
    {
        #region Variables

        [Header("Mesh Renderers")]
        public SkinnedMeshRenderer PlayerMeshRenderer;
        public SkinnedMeshRenderer MirrorMeshRenderer;

        [Header("Shaders")]
        public Shader NormalShader;
        public Shader DissolveShader;

        public MirrorDirection MirrorDirection;
        private MirrorDirection _lastMirrorDirection;
        
        #endregion

        #region Functions

        private void Start()
        {
            UpdateMirror();
        }


        private void LateUpdate()
        {
            if (MirrorDirection != _lastMirrorDirection)
            {
                _lastMirrorDirection = MirrorDirection;
                UpdateMirror();
            }
        }


        private void UpdateMirror()
        {
            if (MirrorDirection == MirrorDirection.NONE)
            {
                PlayerMeshRenderer.material.shader = NormalShader;
                MirrorMeshRenderer.material.shader = NormalShader;
                MirrorMeshRenderer.material.SetInt("IsVisible", 2);
            }
            else
            {
                PlayerMeshRenderer.material.shader = DissolveShader;
                MirrorMeshRenderer.material.shader = DissolveShader;

                UpdateShader();
            }
        }


        private void UpdateShader()
        {
            int direction = MirrorDirection == MirrorDirection.LEFT_TO_RIGHT ? -1 : 1;
            PlayerMeshRenderer.material.SetInt("Direction", direction);
            MirrorMeshRenderer.material.SetInt("Direction", -direction);
        }

        #endregion
    }
}
