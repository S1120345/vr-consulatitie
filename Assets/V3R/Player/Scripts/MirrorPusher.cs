﻿// Copyright (c) 2021 V3RLabs
// Created By: Jelle van Ommen

using System.Collections.Generic;
using UnityEngine;

namespace V3R.MirrorRoom.Player
{
    public class MirrorPusher : MonoBehaviour
    {
        #region Variables

        [SerializeField] private Transform _mirrorPoint;
        [SerializeField] private Transform _playerModel;
        [SerializeField] private Transform _mirrorModel;

        private readonly List<Transform> _fromTransforms = new List<Transform>();
        private List<Transform> _toTransforms = new List<Transform>();

        #endregion

        #region Functions

        private void Start()
        {
            TransformHelper.GetAllChildren(_fromTransforms, _playerModel);
            TransformHelper.GetAllChildren(_toTransforms, _mirrorModel);
        }


        private void LateUpdate()
        {
            MirrorModel();
        }


        private void MirrorModel()
        {
            //Loop over transforms
            for (var i = 0; i < _fromTransforms.Count; i++)
            {
                //Mirror Position
                _toTransforms[i].localPosition = _fromTransforms[i].localPosition;
                _toTransforms[i].localRotation = _fromTransforms[i].localRotation;
            }
        }

        #endregion
    }
}
