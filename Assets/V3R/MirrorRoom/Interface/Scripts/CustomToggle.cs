﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace V3R.MirrorRoom.Interface
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Toggle))]
    public class CustomToggle : MonoBehaviour
    {
        #region Variables

        [Tooltip("Color of button contents when button is active/toggled.")]
        [SerializeField]
        private Color _activeColor = Color.white;

        [Tooltip("Color of button contents when button is in its default state.")]
        [SerializeField] private Color _defaultColor = new Color32(0, 0, 0, 204);

        [SerializeField] private string[] _toggleString = { "Uitgeschakeld", "Ingeschakeld" };

        [SerializeField] private Image[] _icons = new Image[2];

        private Toggle _toggle;
        private TextMeshProUGUI _text;

        #endregion


        #region Functions

        private void Start()
        {
            _toggle = GetComponent<Toggle>();
            _text = GetComponentInChildren<TextMeshProUGUI>();

            //UpdateToggle(false);
            _toggle.onValueChanged.AddListener(UpdateToggle);
        }


        /// <summary>
        /// Update toggle attributes depending on the on/off state.
        /// </summary>
        private void UpdateToggle(bool isOn)
        {
            _text.color = isOn ? _activeColor : _defaultColor;

            if (_icons != null)
            {
                foreach (Image icon in _icons)
                    icon.color = isOn ? _activeColor : _defaultColor;

                _icons[0].gameObject.SetActive(isOn == false);
                _icons[1].gameObject.SetActive(isOn);
            }

            _text.text = _toggleString[isOn ? 1 : 0];
        }

        #endregion
    }
}
