﻿using UnityEngine;

namespace V3R.MirrorRoom.Interface
{
    public class MirrorMenuToggle : MonoBehaviour
    {
        #region Variables

        [SerializeField] private GameObject _mirrorMenuGameObject;
        [SerializeField] private bool _menuVisible = false;

        #endregion


        #region Functions

        private void Start()
        {
            //set menu to given visibility
            _mirrorMenuGameObject.SetActive(_menuVisible);
        }


        public void ToggleMenuVisibility()
        {
            _menuVisible = !_menuVisible;

            _mirrorMenuGameObject.SetActive(_menuVisible);
        }

        #endregion
    }
}