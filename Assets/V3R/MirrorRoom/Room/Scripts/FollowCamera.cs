﻿using UnityEngine;

namespace V3R.MirrorRoom.Room
{
    /// <summary> Place this script on a gameobject to make it follow the camera </summary>
    /// <remarks> Do not manually modify the transform of the GameObject this is on, modify the child instead </remarks>
    public class FollowCamera : MonoBehaviour
    {
        #region Variables

        [Tooltip("Speed at which GameObject follows the camera\n0 doesn't follow, 1 is as fast as possible")]
        [Range(0f, 1f)]
        [SerializeField] private float _cameraFollowSpeed = 1f;

        [Tooltip("Angle required before object starts to follow camera")]
        [SerializeField] private float _minimumAngle;

        [Tooltip("GameObject will keep this distance from camera")]
        [SerializeField] private Vector3 _cameraOffset;

        [SerializeField] private bool _lockPitch;
        [SerializeField] private bool _lockYaw;
        [SerializeField] private bool _lockRoll;

        private Transform _cameraTransform;

        #endregion


        #region Functions

        private void Start()
        {
            //TODO DELETE
           // _cameraTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<IPlayer>().Camera.transform;
          //  transform.position = _cameraTransform.position;
        }


        private void Update()
        {
            transform.localPosition = _cameraTransform.localPosition + _cameraOffset;
            Vector3 cameraEuler = _cameraTransform.rotation.eulerAngles;
            if (_lockPitch) { cameraEuler.x = transform.rotation.eulerAngles.x; }
            if (_lockYaw) { cameraEuler.y = transform.rotation.eulerAngles.y; }
            if (_lockRoll) { cameraEuler.z = transform.rotation.eulerAngles.z; }
            if (Quaternion.Angle(transform.rotation, Quaternion.Euler(cameraEuler)) >= _minimumAngle) { transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(cameraEuler), _cameraFollowSpeed); }
        }

        #endregion
    }
}