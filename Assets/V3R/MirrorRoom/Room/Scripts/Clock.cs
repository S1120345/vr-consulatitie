﻿using System;
using UnityEngine;

namespace V3R.MirrorRoom.Room
{
    public class Clock : MonoBehaviour
    {
        #region Variables

        [SerializeField] private GameObject _hourHand;
        [SerializeField] private GameObject _minuteHand;
        private AudioSource _audioSrc;
        private DateTime _displayTime;

        #endregion


        #region Functions

        private void Start()
        {
            // Find audio and start it
            //_audioSrc = GetComponent<AudioSource>();
            // _audioSrc.Play();
        }


        private void Update()
        {
            // Update clock if the current minute or hour is not the same as the displayed minute
            if (_displayTime.Minute != DateTime.Now.Minute || _displayTime.Hour != DateTime.Now.Hour) { UpdateClock(); }
        }


        /// <summary>
        /// Function which updates the rotation of the minutehand and hourhand to match the current time
        /// </summary>
        private void UpdateClock()
        {
            _displayTime = DateTime.Now;
            float minuteRotation = (float)_displayTime.Minute / 60 * 360;
            float hourRotation = ((float)_displayTime.Hour % 12 + (float)_displayTime.Minute / 60) / 12 * 360;
            _minuteHand.gameObject.transform.localEulerAngles = new Vector3(0, -90, minuteRotation);
            _hourHand.gameObject.transform.localEulerAngles = new Vector3(0, -90, hourRotation);
        }

        #endregion
    }
}