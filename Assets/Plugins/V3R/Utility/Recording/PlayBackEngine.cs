﻿using System;
using System.Timers;
using UnityEngine;

namespace V3R.Utility.Recording
{
    public class PlayBackEngine : MonoBehaviour
    {
        #region Variables

        private RecordingData _recordingData;
        private Transform[] _transforms;

        private bool _playRecording;
        private int _recordingIndex;
        [NonSerialized]
        //  Timer makes it so the recording stops for 3 seconds and then start again
        public Timer timer;
        #endregion

        #region Functions

        private void Start()
        {

        }


        private void Update()
        {

        }


        private void FixedUpdate()
        {
            PlayBackTick();
        }

        private void PlayBackTick()
        {
            if (_recordingData != null && _playRecording)
            {
                //Next row
                if (_recordingIndex < _recordingData.Data.Count)
                {
                    PushTransforms(_recordingIndex);

                    _recordingIndex++;
                }
                //Finish playback
                else
                {
                    _playRecording = false;
                    _recordingIndex = 0;
                    // setup timer and start it
                    timer = new Timer(3000) {AutoReset = false};
                    timer.Elapsed +=
                        (sender, e) => _playRecording = true;
                    timer.Start();
                }
            }
        }

        private void PushTransforms(int index)
        {
            for (var i = 0; i < _transforms.Length; i++)
            {
                if (i >= 0 && _recordingData.Data[index].Length > i)
                {
                    TransformPoint transformPoint = _recordingData.Data[index][i];

                    _transforms[i].position = transformPoint.Position;
                    _transforms[i].rotation = transformPoint.Rotation;
                }
            }
        }


        #region PlayBack Controls

        public bool IsPlaying() =>
            _recordingData != null && _playRecording;


        public void SetRecordingData(RecordingData data, Transform[] transforms)
        {
            Debug.Log("Setting recording:");
            Debug.Log($"Rows: {data.Data.Count}");
            Debug.Log(transforms.Length);

            _recordingData = data;
            _transforms = transforms;
        }


        public void StartRecording()
        {
            if (_playRecording)
            {
                StopRecording();
                StartRecording();
            }
            else
            {
                _playRecording = true;
            }
            
        }


        public void SetRecordingIndex(int index)
        {
            PushTransforms(index);
        }


        public void StopRecording() {
            _playRecording = false;
            SetRecordingIndex(0);
            timer.Close();
        }

        #endregion





        #endregion
    }
}
