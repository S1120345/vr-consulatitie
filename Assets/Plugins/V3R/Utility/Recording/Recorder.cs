﻿using UnityEngine;

namespace V3R.Utility.Recording
{
    public class Recorder
    {
        #region Variables

        public RecordingData RecordingData;

        private Transform[] _transforms;
        
        #endregion

        #region Functions

        public Recorder(Transform[] transforms, RecorderSettings recorderSettings)
        {
            _transforms = transforms;
            RecordingData = new RecordingData(recorderSettings);
        }


        public void Update()
        {
            TransformPoint[] row = RecordDataRow();

            if(row != null)
                RecordingData.Data.Add(row);
        }


        private TransformPoint[] RecordDataRow()
        {
            var row = new TransformPoint[_transforms.Length];

            for (var i = 0; i < _transforms.Length; i++)
            {
                Transform t = _transforms[i];
                var tPoint = new TransformPoint
                {
                    Position = RecordingData.RecorderSettings.PositionSpace == Space.WORLD ? t.position : t.localPosition,
                    Rotation = RecordingData.RecorderSettings.RotationSpace == Space.WORLD ? t.rotation : t.localRotation
                };
                row[i] = tPoint;
            }

            return row;
        }

        #endregion
    }
}