﻿
using System;

namespace V3R.Utility
{
    [Serializable]
    public enum Space {WORLD, LOCAL}

    [Serializable]
    public class RecorderSettings
    {
        public int MaxRows;

        public Space PositionSpace;
        public Space RotationSpace;
        public Space ScaleSpace;
    }
}
