﻿using System;
using System.Collections.Generic;
using V3R.Common.Serialization;

namespace V3R.Utility.Recording
{
    [Serializable]
    public struct TransformPoint
    {
        public SerializableVector3 Position;
        public SerializableQuaternion Rotation;
    }


    [Serializable]
    public class RecordingData
    {
        public RecorderSettings RecorderSettings;
        public List<TransformPoint[]> Data;


        public RecordingData(RecorderSettings recorderSettings)
        {
            RecorderSettings = recorderSettings;
            Data = new List<TransformPoint[]>();
        }
    }
}
