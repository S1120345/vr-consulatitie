﻿using System;
using UnityEngine;

namespace V3R.Common.Serialization
{
    [Serializable]
    public class SerializablePoint
    {
        public SerializableVector3 Position;
        public SerializableQuaternion Rotation;

        public SerializablePoint()
        {

        }

        public SerializablePoint(Vector3 position, Quaternion rotation)
        {
            Position = position;
            Rotation = rotation;
        }
    }
}
