﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;

namespace V3R.Common
{
    public static class ExtensionMethods
    {
        //Capatilize the first letter
        public static string Capitalize(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return string.Empty;

            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }

        public static string FromUTF(this byte[] data) =>
            System.Text.Encoding.UTF8.GetString(data);


        #region Dictionary

        /// <summary>
        /// Add or update a value in the dictionary
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <param name="t"></param>
        public static void AddOrUpdate<TKey, T>(this Dictionary<TKey, T> dictionary, TKey key, T t)
        {
            if (dictionary.ContainsKey(key))
                dictionary[key] = t;
            else
                dictionary.Add(key, t);
        }


        /// <summary>
        /// Remove a key from the dictionary if it exists
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        public static void RemoveSafely<TKey, T>(this Dictionary<TKey, T> dictionary, TKey key)
        {
            if (dictionary.ContainsKey(key))
                dictionary.Remove(key);
        }


        /// <summary>
        /// Get a value from the dictionary or default
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="T"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Get<TKey, T>(this Dictionary<TKey, T> dictionary, TKey key) =>
            dictionary.ContainsKey(key) ? dictionary[key] : default;

        #endregion

        #region Transform

        #region Strict Find()

        //Breadth-first search
        public static Transform FindChildBreadth(this Transform parent, string name)
        {
            var queue = new Queue<Transform>();
            queue.Enqueue(parent);
            while (queue.Count > 0)
            {
                Transform child = queue.Dequeue();
                if (child.name == name)
                    return child;
                foreach (Transform t in child)
                    queue.Enqueue(t);
            }
            return null;
        }


        //Depth-first search
        public static Transform FindChildDepth(this Transform aParent, string name)
        {
            foreach (Transform child in aParent)
            {
                if (child.name == name)
                    return child;
                Transform result = child.FindChildDepth(name);
                if (result != null)
                    return result;
            }
            return null;
        }

        #endregion

        #endregion

        #region GameObject

        public static T FindComponent<T>(this GameObject gameObj) where T : Component
        {
            var component = gameObj.GetComponent<T>();

            if (component == null)
                component = gameObj.GetComponentInChildren<T>();

            return component;
        }

        #endregion

    }
}
