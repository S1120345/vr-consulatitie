﻿
using UnityEngine;

namespace V3R.Common
{
    public static class VectorUtil
    {
        public static Vector3 Random(float min, float max)
        {
            return new Vector3(
                UnityEngine.Random.Range(min, max),
                UnityEngine.Random.Range(min, max),
                UnityEngine.Random.Range(min, max));
        }

        public static Vector3 Random(float range)
        {
            return new Vector3(
                UnityEngine.Random.Range(-range, range),
                UnityEngine.Random.Range(-range, range),
                UnityEngine.Random.Range(-range, range));
        }
    }
}
