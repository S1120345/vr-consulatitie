﻿using UnityEngine;

namespace V3R.Common
{
    public static class ParseFunctions
    {
        /// <summary>
        /// Parse string to Vector3
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Vector3 ParseVector3(this string str, char seperator)
        {
            string[] s = str.Substring(1, str.Length - 2).Split(seperator);

            float x = float.Parse(s[0]);
            float y = float.Parse(s[1]);
            float z = float.Parse(s[2]);

            return new Vector3(x, y, z);
        }


        /// <summary>
        /// Parse string to Quaternion
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Quaternion ParseQuaternion(this string str, char seperator)
        {
            string[] s = str.Substring(1, str.Length - 2).Split(seperator);

            float x = float.Parse(s[0]);
            float y = float.Parse(s[1]);
            float z = float.Parse(s[2]);
            float w = float.Parse(s[3]);

            return new Quaternion(x, y, z, w);
        }
    }
}
