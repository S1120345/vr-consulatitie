﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace V3R.Common
{
    public static class QuaternionUtil
    {

        public static Quaternion Random(float min, float max)
        {
            return new Quaternion(
                UnityEngine.Random.Range(min, max),
                UnityEngine.Random.Range(min, max),
                UnityEngine.Random.Range(min, max),
                UnityEngine.Random.Range(min, max));
        }


        public static Quaternion Random(float range)
        {
            return new Quaternion(
                UnityEngine.Random.Range(-range, range),
                UnityEngine.Random.Range(-range, range),
                UnityEngine.Random.Range(-range, range),
                UnityEngine.Random.Range(-range, range));
        }

    }
}
