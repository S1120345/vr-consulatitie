﻿using System;
using UnityEngine;
using V3R.Common.Serialization;

namespace V3R.Common
{
    [Serializable]
    public class V3RTransform
    {
        public SerializableVector3 Position;
        public SerializableQuaternion Rotation;

        public V3RTransform()
        {

        }

        public V3RTransform(Vector3 position, Quaternion rotation)
        {
            Position = position;
            Rotation = rotation;
        }
    }
}
