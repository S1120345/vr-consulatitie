﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace V3R.Common
{
    public class UnityMainThread : MonoBehaviour
    {
        #region Variables

        private static Queue<Action> _jobs = new Queue<Action>();

        #endregion

        #region Functions

        private void Update()
        {
            while (_jobs.Count > 0)
            {
                _jobs.Dequeue()?.Invoke();
            }
        }

        public static void AddJob(Action job)
        {
            _jobs.Enqueue(job);
        }

        #endregion
    }
}
