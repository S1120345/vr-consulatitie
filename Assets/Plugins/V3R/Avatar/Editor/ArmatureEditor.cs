﻿using UnityEditor;
using V3R.Avatar.Tracking;

namespace V3R.Avatar.Editor
{
    [CustomEditor(typeof(Armature))]
    public class ArmatureEditor : UnityEditor.Editor
    {
        #region Variables


        #endregion

        #region Functions

        private void OnEnable()
        {

        }


        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
        }

        #endregion
    }
}
