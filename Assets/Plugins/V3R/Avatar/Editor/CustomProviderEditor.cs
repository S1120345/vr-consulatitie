﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using V3R.Avatar.Tracking.Providers;

namespace V3R.Avatar.Editor
{
    [CustomEditor(typeof(CustomProvider))]
    public class CustomProviderEditor : UnityEditor.Editor
    {
        #region Variables

        private SerializedProperty _transformList;
        private ReorderableList _transformReorderableList;

        #endregion

        #region Functions

        private void OnEnable()
        {
            _transformList = serializedObject.FindProperty("Transforms");

            _transformReorderableList = new ReorderableList(serializedObject, _transformList, true, true, true, true)
            {
                drawElementCallback = DrawListItems,
                drawHeaderCallback = DrawHeader
            };
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            _transformReorderableList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }


        private static void DrawHeader(Rect rect) => 
            EditorGUI.LabelField(rect, "Transforms");


        private void DrawListItems(Rect rect, int index, bool isactive, bool isfocused)
        {
            SerializedProperty t = _transformList.GetArrayElementAtIndex(index);

            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, 250, EditorGUIUtility.singleLineHeight),
                t,
                GUIContent.none
            );
        }

        #endregion
    }
}
