﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using V3R.Avatar.Tracking;
using V3R.Avatar.Tracking.Providers;

namespace V3R.Avatar.Editor
{
    [CustomEditor(typeof(TrackerProcessor))]
    public class TrackerProcessorEditor : UnityEditor.Editor
    {
        #region Variables

        private SerializedProperty _trackerProviderModelList;
        private SerializedProperty _armatureProperty;

        private List<bool> _trackerFoldOut;
        private bool _calibrationEventFoldOut;

        private string _editorTheme = "_Light"; //EditorGUIUtility.isProSkin ? "_Dark": "_Light"; TODO Add Textures for Dark Theme
        private string _baseImagePath = "Assets/Plugins/V3R/Avatar/Editor/Images/";

        #endregion

        #region Functions

        private void OnEnable()
        {
            _trackerProviderModelList = serializedObject.FindProperty("TrackerProviders");
            _armatureProperty = serializedObject.FindProperty("Armature");

            //Foldouts
            _trackerFoldOut = new List<bool>();
            for (var i = 0; i < _trackerProviderModelList.arraySize; i++)
                _trackerFoldOut.Add(false);
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            Texture2D imageInspector = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Plugins/V3R/Avatar/Editor/Images/Tracker Processor Banner.png", typeof(Texture2D));
            if (imageInspector)
            {
                float imageHeight = imageInspector.height;
                float imageWidth = imageInspector.width;
                float aspectRatio = imageHeight / imageWidth;
                Rect rect = GUILayoutUtility.GetRect(imageHeight, aspectRatio * Screen.width * 0.7f);
                EditorGUI.DrawTextureTransparent(rect, imageInspector);
            }

            GUILayout.Space(20);

            #region Main Controls

            //Style
            var style = new GUIStyle(EditorStyles.label);
            style.fontSize = 16;

            //Armature Label
            EditorGUI.LabelField(new Rect(20, 60, 120, 20), new GUIContent("Armature"), style);

            //Info Icon
            var infoIcon = (Texture2D)AssetDatabase.LoadAssetAtPath($"Assets/Plugins/V3R/Avatar/Editor/Images/Info{_editorTheme}.png", typeof(Texture2D));
            EditorGUI.LabelField(new Rect(100, 62, 17, 17), new GUIContent(infoIcon, "Pushes the processed bones to the Armature"), style);


            //Armature Property Field
            EditorGUI.PropertyField(new Rect(20, 90, 320, 20), _armatureProperty, new GUIContent());


            //Load & Save Buttons
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            //Load Icon
            var loadIcon = AssetDatabase.LoadAssetAtPath<Texture2D>($"{_baseImagePath}Load{_editorTheme}.png");
            if (GUILayout.Button(new GUIContent(loadIcon, "Load Armature from config"), GUILayout.Width(40), GUILayout.Height(40)))
            {
                ((TrackerProcessor)serializedObject.targetObject).LoadConfig();
            }

            //Save Icon
            var saveIcon = (Texture2D)AssetDatabase.LoadAssetAtPath($"{_baseImagePath}Save{_editorTheme}.png", typeof(Texture2D));


            if (GUILayout.Button(new GUIContent(saveIcon, "Save Armature to config"), GUILayout.Width(40), GUILayout.Height(40)))
            {
                ((TrackerProcessor)serializedObject.targetObject).SaveConfig();
            }
            GUILayout.Space(10);
            GUILayout.EndHorizontal();


            //Calibration Event
            _calibrationEventFoldOut = EditorGUILayout.BeginFoldoutHeaderGroup(_calibrationEventFoldOut, "On Calibrated");

            if (_calibrationEventFoldOut)
            {
                GUILayout.Space(10);
                SerializedProperty calibrationEventProperty = serializedObject.FindProperty("OnCalibrated");
                EditorGUILayout.PropertyField(calibrationEventProperty, true);
            }

            EditorGUILayout.EndFoldoutHeaderGroup();

            #endregion

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            //Providers Label
            EditorGUILayout.LabelField(new GUIContent("Providers"), style);
            GUILayout.Space(5);


            #region Provider Foldouts

            for (var i = 0; i < _trackerProviderModelList.arraySize; i++)
            {
                GetBones(i);

                var trackerProviderModel = _trackerProviderModelList.GetArrayElementAtIndex(i);
                var trackerProvider = trackerProviderModel.FindPropertyRelative("TrackerProvider").objectReferenceValue;
                var label = "None";

                if (trackerProvider != null)
                    label = trackerProvider.GetType().Name;

                GUILayout.BeginHorizontal();
                _trackerFoldOut[i] = EditorGUILayout.BeginFoldoutHeaderGroup(_trackerFoldOut[i], label);

                EditorGUILayout.EndFoldoutHeaderGroup();

                if (GUILayout.Button("v", GUILayout.Width(20)))
                {
                    if (i < _trackerProviderModelList.arraySize - 1)
                    {
                        _trackerProviderModelList.MoveArrayElement(i, i + 1);
                        var temp = _trackerFoldOut[i];
                        _trackerFoldOut[i] = _trackerFoldOut[i + 1];
                        _trackerFoldOut[i + 1] = temp;
                    }

                }

                if (GUILayout.Button("^", GUILayout.Width(20)))
                {
                    if (i > 0)
                    {
                        _trackerProviderModelList.MoveArrayElement(i, i - 1);
                        var temp = _trackerFoldOut[i];
                        _trackerFoldOut[i] = _trackerFoldOut[i - 1];
                        _trackerFoldOut[i - 1] = temp;
                    }
                }
                GUILayout.EndHorizontal();

                if (_trackerFoldOut[i])
                    DrawProviders(trackerProviderModel, i);
            }


            //Draw Provider Add Button
            GUILayout.Space(10);
            if (GUILayout.Button("Add Provider"))
            {
                _trackerFoldOut.Add(false);
                _trackerProviderModelList.arraySize++;
            }

            #endregion

            serializedObject.ApplyModifiedProperties();
        }


        #region Tracker Provider List

        private void DrawProviders(SerializedProperty trackerProviderModel, int index)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.ObjectField(trackerProviderModel.FindPropertyRelative("TrackerProvider"), GUIContent.none);

            if (GUILayout.Button("Delete"))
            {
                if (EditorUtility.DisplayDialog(
                    "Delete Provider?",
                    "Deleting the provider can not be reversed",
                    "Confirm",
                    "Cancel"))
                {
                    _trackerFoldOut.RemoveAt(index);
                    _trackerProviderModelList.DeleteArrayElementAtIndex(index);
                    return;
                }
            }
            GUILayout.EndHorizontal();

            SerializedProperty bones = trackerProviderModel.FindPropertyRelative("Bones");

            for (var i = 0; i < bones.arraySize; i++)
            {
                SerializedProperty serializedBonePair = bones.GetArrayElementAtIndex(i);

                SerializedProperty syncPosition = serializedBonePair.FindPropertyRelative("SyncPosition");
                SerializedProperty syncRotation = serializedBonePair.FindPropertyRelative("SyncRotation");
                SerializedProperty trackerBone = serializedBonePair.FindPropertyRelative("FromTransform");
                SerializedProperty avatarBone = serializedBonePair.FindPropertyRelative("ToTransform");

                EditorGUILayout.BeginHorizontal();
                EditorGUI.BeginChangeCheck();

                EditorGUILayout.PropertyField(syncPosition, GUIContent.none, GUILayout.Width(12));
                EditorGUILayout.PropertyField(syncRotation, GUIContent.none, GUILayout.Width(12));

                //From Bone (Disabled)
                EditorGUI.BeginDisabledGroup(true);
                EditorGUILayout.ObjectField(trackerBone, typeof(Transform), GUIContent.none, GUILayout.Width(170));
                EditorGUI.EndDisabledGroup();

                EditorGUILayout.ObjectField(avatarBone, typeof(Transform), GUIContent.none, GUILayout.Width(200));

                EditorGUI.EndChangeCheck();
                EditorGUILayout.EndHorizontal();
            }

            GUILayout.Space(10);
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
        }


        private void GetBones(int index)
        {
            SerializedProperty trackerProviderModel = _trackerProviderModelList.GetArrayElementAtIndex(index);

            //Tracker Reference
            SerializedProperty trackerProvider = trackerProviderModel.FindPropertyRelative("TrackerProvider");

            var provider = (TrackerProvider)trackerProvider.objectReferenceValue;
            var trackerProcessor = (TrackerProcessor)serializedObject.targetObject;

            if (provider == null)
            {
                trackerProcessor.TrackerProviders[index].Bones = new BonePair[0];
                return;
            }

            var bones = provider.GetProviderTransforms();
            var newArray = new BonePair[bones.Count];

            var bonePairArray = trackerProcessor.TrackerProviders[index].Bones;


            for (var i = 0; i < bones.Count; i++)
            {
                if (i < bonePairArray.Length)
                    newArray[i] = bonePairArray[i];
                else
                    newArray[i] = new BonePair();
                newArray[i].FromTransform = bones[i];
            }

            trackerProcessor.TrackerProviders[index].Bones = newArray;
        }

        #endregion

        #endregion
    }
}