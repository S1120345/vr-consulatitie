﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace V3R.Avatar.Tracking.Adapters
{
    public class TrackerAdapter : MonoBehaviour
    {
        public virtual List<Transform> GetAdapterTransforms()
        {
            return null;
        }
    }


    [Serializable]
    public class TrackerAdapterModel
    {
        public TrackerAdapter TrackerAdapter;
        public BonePair[] Bones;
    }

    [Serializable]
    public class BonePair
    {
        public Transform FromBone;
        public Transform ToBone;

        public BonePair() { }


        public BonePair(Transform fromBone, Transform toBone)
        {
            FromBone = fromBone;
            ToBone = toBone;
        }
    }
}
