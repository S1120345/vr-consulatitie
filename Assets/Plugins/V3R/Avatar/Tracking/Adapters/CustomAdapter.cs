﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace V3R.Avatar.Tracking.Adapters
{
    public class CustomAdapter : TrackerAdapter
    {
        #region Variables

        public Transform[] Transforms;

        #endregion

        #region Functions

        public override List<Transform> GetAdapterTransforms()
        {
            return Transforms.ToList();
        }

        #endregion


    }
}
