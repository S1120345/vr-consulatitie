﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using V3R.Common;

namespace V3R.Avatar.Tracking
{
    public enum TransformMode { DIRECT, TOWARDS }

    public class Armature : MonoBehaviour
    {
        #region Variables

        public ArmatureConfig ArmatureConfig;
        public TransformMode TransformMode;

        private List<Transform> _bones;

        #endregion

        #region Functions

        protected void OnEnable()
        {
            _bones = LoadBonesFromConfig();
        }


        public virtual List<Transform> GetTransforms() =>
            _bones;


        public virtual void PushTransforms(List<V3RTransform> points)
        {
            for (var i = 0; i < _bones.Count; i++)
            {
                if (_bones[i] is null)
                    continue;

                Transform t = _bones[i];

                //Position
                if (ArmatureConfig.Bones[i].SyncPosition)
                {
                    if (TransformMode == TransformMode.DIRECT)
                        t.position = points[i].Position;
                    else
                    {
                        float distance = Vector3.Distance(t.position, points[i].Position);

                        t.position = Vector3.MoveTowards(
                            t.position,
                            points[i].Position,
                            distance * Time.deltaTime);
                    }
                }

                //Rotation
                if (ArmatureConfig.Bones[i].SyncRotation)
                {
                    if (TransformMode == TransformMode.DIRECT)
                        _bones[i].rotation = points[i].Rotation;
                    else
                    {
                        float angle = Quaternion.Angle(t.rotation, points[i].Rotation);

                        t.rotation = Quaternion.RotateTowards(
                            t.rotation,
                            points[i].Rotation,
                            (angle * Time.deltaTime) * 5);
                    }
                }
            }
        }


        #region Config

#if UNITY_EDITOR
        /// <summary>
        /// Save a collection of BonePairs to the armature config
        /// </summary>
        /// <param name="bonePairs"></param>
        public virtual void SaveBonesToConfig(IEnumerable<BonePair> bonePairs)
        {
            var boneConfigs = new List<BoneConfig>();

            foreach (var bonePair in bonePairs)
            {
                if (bonePair.ToTransform == null)
                    continue;

                var boneConfig = new BoneConfig();

                boneConfig.SyncPosition = bonePair.SyncPosition;
                boneConfig.SyncRotation = bonePair.SyncRotation;
                boneConfig.FromTransform = bonePair.FromTransform.name;
                
                var currentTransform = bonePair.ToTransform;
                var transformPath = currentTransform.name;

                while (currentTransform != transform)
                {
                    if (currentTransform.parent == null)
                        break;
                    currentTransform = currentTransform.parent;
                    transformPath = currentTransform.name + "/" + transformPath;
                }
                
                boneConfig.ToTransform = transformPath;
                boneConfigs.Add(boneConfig);
            }

            ArmatureConfig.Bones = boneConfigs;

            EditorUtility.SetDirty(ArmatureConfig);
            AssetDatabase.SaveAssets();
        }
#endif

        /// <summary>
        /// Loop through the list of paths in the armature config and find the corrosponding transforms
        /// </summary>
        /// <returns></returns>
        public virtual List<Transform> LoadBonesFromConfig()
        {
            var bones = new List<Transform>();
            
            foreach (var boneConfig in ArmatureConfig.Bones)
            {
                var bonePath = boneConfig.ToTransform.Split('/');
                var currentTransform = transform;

                for (var i = 1; i < bonePath.Length; i++)
                {
                    currentTransform = currentTransform.Find(bonePath[i]);
                }

                bones.Add(currentTransform);
            }

            return bones;
        }

        #endregion

        #endregion
    }
}
