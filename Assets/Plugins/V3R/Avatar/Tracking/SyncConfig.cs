﻿using System;

namespace V3R.Avatar.Tracking
{
    [Serializable]
    public class SyncConfig
    {
        public bool SyncPosition;
        public bool SyncRotation;
    }
}
