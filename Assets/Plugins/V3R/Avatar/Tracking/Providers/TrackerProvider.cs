﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace V3R.Avatar.Tracking.Providers
{
    public abstract class TrackerProvider : MonoBehaviour
    {
        public abstract List<Transform> GetProviderTransforms();
    }


    [Serializable]
    public class TrackerProviderModel
    {
        public TrackerProvider TrackerProvider;
        public BonePair[] Bones;
    }
}
