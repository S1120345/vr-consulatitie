﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace V3R.Avatar.Tracking.Providers
{
    public class CustomProvider : TrackerProvider
    {
        #region Variables

        public Transform[] Transforms;

        #endregion

        #region Functions

        public override List<Transform> GetProviderTransforms()
        {
            return Transforms.ToList();
        }

        #endregion
    }
}
