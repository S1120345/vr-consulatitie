﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace V3R.Avatar.Tracking
{
    [Serializable]
    [CreateAssetMenu(fileName = "Armature Config", menuName = "V3R/Armature Config", order = 0)]
    public class ArmatureConfig : ScriptableObject
    {
        public List<BoneConfig> Bones;
    }


    [Serializable]
    public class BonePair : SyncConfig
    {
        public Transform FromTransform;
        public Transform ToTransform;

        public BonePair() { }


        public BonePair(Transform fromTransform, Transform toTransform)
        {
            FromTransform = fromTransform;
            ToTransform = toTransform;
        }
    }

    
    [Serializable]
    public class BoneConfig : SyncConfig
    {
        public string FromTransform;
        public string ToTransform;
    }
}
