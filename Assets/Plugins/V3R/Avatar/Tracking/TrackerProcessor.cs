﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using V3R.Avatar.Tracking.Providers;
using V3R.Common;

namespace V3R.Avatar.Tracking
{
    public class TrackerProcessor : MonoBehaviour
    {
        #region Variables

        public TrackerProviderModel[] TrackerProviders;
        public UnityEvent OnCalibrated;

        public Armature Armature;

        private List<Transform> _trackerArmature;

        #endregion

        #region Functions

        private void Start()
        {
            _trackerArmature = new List<Transform>();

            Calibrate();
        }


        private void OnDisable()
        {
            foreach (Transform trackerTransform in _trackerArmature.Where(trackerTransform => trackerTransform != null))
                Destroy(trackerTransform.gameObject);
        }


        private void LateUpdate()
        {
            var points = GetBoneV3RTransforms();

            if (Armature != null)
                Armature.PushTransforms(points);
        }


        private void Calibrate()
        {
            if (TrackerProviders == null)
                return;

            foreach (TrackerProviderModel trackerAdapterModel in TrackerProviders)
            {
                foreach (BonePair trackerBonePair in trackerAdapterModel.Bones)
                {
                    if (trackerBonePair != null && trackerBonePair.ToTransform != null)
                    {
                        var virtualBoneObject = new GameObject($"Virtual_{trackerBonePair.FromTransform.name}");
                        virtualBoneObject.transform.SetParent(trackerBonePair.FromTransform);

                        virtualBoneObject.transform.position = trackerBonePair.FromTransform.position;
                        virtualBoneObject.transform.rotation = trackerBonePair.ToTransform.rotation;

                        trackerBonePair.FromTransform = virtualBoneObject.transform;

                        _trackerArmature.Add(virtualBoneObject.transform);
                    }
                }
            }

            OnCalibrated.Invoke();
        }

        #region Armature Processing

        private List<V3RTransform> GetBoneV3RTransforms()
        {
            var boneList = new List<V3RTransform>();
            foreach (var trackerAdapterModel in TrackerProviders)
            {
                foreach (var bonePair in trackerAdapterModel.Bones)
                {
                    if (bonePair.FromTransform == null || bonePair.ToTransform == null)
                        continue;

                    var bone = new V3RTransform(bonePair.FromTransform.position, bonePair.FromTransform.rotation);
                    boneList.Add(bone);
                }
            }

            return boneList;
        }

        #endregion

        #region Armature Config

#if UNITY_EDITOR
        /// <summary>
        /// Save all bones to the config on the Armature
        /// </summary>
        public void SaveConfig()
        {
            var bonePairs = new List<BonePair>();

            foreach (TrackerProviderModel trackerAdapterModel in TrackerProviders)
                bonePairs.AddRange(trackerAdapterModel.Bones);

            Armature.SaveBonesToConfig(bonePairs);
        }
#endif

        /// <summary>
        /// Load all bones from the config on the Armature
        /// </summary>
        public void LoadConfig()
        {
            List<Transform> transforms = Armature.LoadBonesFromConfig();
            List<BoneConfig> bones = Armature.ArmatureConfig.Bones;

            var counter = 0;
            foreach (TrackerProviderModel trackerProviderModel in TrackerProviders)
            {
                foreach (BonePair bonePair in trackerProviderModel.Bones)
                {
                    if (counter >= bones.Count)
                        break;
                    
                    if (bonePair.FromTransform.name == bones[counter].FromTransform)
                    {
                        bonePair.SyncPosition = bones[counter].SyncPosition;
                        bonePair.SyncRotation = bones[counter].SyncRotation;
                        bonePair.ToTransform = transforms[counter];
                        counter++;
                    }
                }
            }
        }

        #endregion

        #endregion
    }
}
