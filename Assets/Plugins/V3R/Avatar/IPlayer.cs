﻿using UnityEngine;

namespace V3R.Avatar
{
    public interface IPlayer
    {
        GameObject GameObject { get; }
        Camera Camera { get; }
    }
}
