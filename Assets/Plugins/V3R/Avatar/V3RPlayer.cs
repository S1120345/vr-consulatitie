﻿using UnityEngine;

namespace V3R.Avatar
{
    public class V3RPlayer : MonoBehaviour, IPlayer
    {
        #region Variables

        [SerializeField] private Camera _camera;

        public GameObject GameObject => gameObject;

        public Camera Camera
        {
            get => _camera;
            set => _camera = value;
        }
        
        #endregion
    }
}
