using Nakama;
using UnityEngine;

namespace V3R.Multiplayer
{
    public class RealtimeSocketConnection
    {
        #region variables
        
        //general variables
        private IClient _client;
        private ISession _session;
        public ISocket socket;

        #endregion
        

        #region functions
        
        /// <summary>
        /// constructor
        /// </summary>>
        public RealtimeSocketConnection(IClient c, ISession s)
        {
            _client = c;
            _session = s;
            socket = _client.NewSocket();
            Connect();
        }

        
        /// <summary>
        /// async function to connect to session
        /// </summary>>
        private async void Connect()
        {
            socket.Connected += () => Debug.Log("Socket connected");
            socket.Closed += () => Debug.Log("Socket closed");
            await socket.ConnectAsync(_session);
        }

        #endregion
        
        
        
    }
}