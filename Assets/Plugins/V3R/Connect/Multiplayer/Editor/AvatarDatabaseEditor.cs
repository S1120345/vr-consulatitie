﻿using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace V3R.Connect.Multiplayer.Editor
{
    [CustomEditor(typeof(AvatarDatabase))]
    public class AvatarDatabaseEditor : UnityEditor.Editor
    {
        #region Variables

        private SerializedProperty _playerPrefabList;
        private ReorderableList _playerPrefabReorderableList;

        #endregion

        #region Functions

        private void OnEnable()
        {
            _playerPrefabList = serializedObject.FindProperty("AvatarPrefabs");

            _playerPrefabReorderableList = new ReorderableList(serializedObject, _playerPrefabList, true, true, true, true)
            {
                drawElementCallback = DrawListItems,
                drawHeaderCallback = DrawListHeader
            };
        }


        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            
            _playerPrefabReorderableList.DoLayoutList();
            
            serializedObject.ApplyModifiedProperties();
        }


        private void DrawListHeader(Rect rect) =>
            EditorGUI.LabelField(rect, "Player Prefabs");


        private void DrawListItems(Rect rect, int index, bool isactive, bool isfocused)
        {
            SerializedProperty avatarPrefab = _playerPrefabList.GetArrayElementAtIndex(index);
            
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, 250, EditorGUIUtility.singleLineHeight),
                avatarPrefab,
                GUIContent.none
            );
        }

        #endregion
    }
}
