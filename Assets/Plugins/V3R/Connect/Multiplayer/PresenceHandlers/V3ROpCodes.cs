﻿namespace V3R.Connect.Multiplayer
{
    public class V3ROpCodes
    {
        public const int PLAYER_INFO = 2;
        public const int ARMATURE = 3;
        public const int LOOKIE = 4;
        public const int GHOSTHANDS = 5;
        public const int MIRROR = 6;
    }
}
