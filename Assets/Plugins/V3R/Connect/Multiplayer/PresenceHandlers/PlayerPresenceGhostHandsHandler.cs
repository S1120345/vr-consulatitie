﻿using Nakama;
using Plugins.V3R.Connect.Multiplayer.PresenceHandlers;
using UnityEngine;
using V3R.Common;
using V3R.Connect.Multiplayer;
using V3R.Multiplayer;
using V3R.Utility.Recording;

public class PlayerPresenceGhostHandsHandler : MonoBehaviour
{
    #region Variables

    [SerializeField] public MatchSys multiplayer;
    [SerializeField] public PlayerPresenceHandler playerPresenceHandler;
    public PlayBackEngine PlayBackEngine;
    [SerializeField] public Ghosthands ghosthands;
    [SerializeField] public Ghosthands Remoteghosthands;
    private bool _receiving = false;
    private float _time = 1f / 30f; //Update time, 30 Ticks per second
    private string UserID;
    private bool update = false;

    #endregion

    #region Functions

    private void Start()
    {
        multiplayer.AddStateListener(V3ROpCodes.GHOSTHANDS, UpdatePlayerLookie);
        UserID = gameObject.GetComponent<SessionWithPreferences>().session.UserId;
    }


    private void Update()
    {
        if (_time <= 0)
        {
            _time = 1f / 30f;

            if (ghosthands is null)
                return;

            var dataString = "";
            
            var transforms = ghosthands.Transforms;

            if (ghosthands.active)
            {
                update = true;
                dataString += 1;
            }
            else
            {
                update = false;
                dataString += 0;
            }

            if (PlayBackEngine.IsPlaying()) 
                foreach (var transform in transforms)
                {
                    var nextVector =
                        TransformCodec.EncodeVector3(new[] {transform.position});
                    dataString += nextVector;

                    var nextQuaternion =
                        TransformCodec.EncodeQuaternion(new[] {transform.rotation});
                    dataString += nextQuaternion;
                }

            multiplayer.SendMatchState(V3ROpCodes.GHOSTHANDS, dataString);
        }

        _time -= Time.deltaTime;
    }


    private void UpdatePlayerLookie(IMatchState matchState)
    {
        if (matchState.UserPresence.UserId == UserID)
            return;
        UnityMainThread.AddJob(() =>
        {
            var playerObject = Remoteghosthands.gameObject;
            var content = matchState.State.FromUTF();
            var ghosthand = playerObject.GetComponent<Ghosthands>();

            if (content.StartsWith("0"))
            {
                playerObject.SetActive(false);
                
                return;
            }
            playerObject.SetActive(true);
            _receiving = true;
            content = content.Remove(0, 1);
            if (content.Length > 0)
            {
                foreach (var playerTransform in ghosthand.Transforms)
                {
                    var v3rTransform = new V3RTransform();

                    //Position
                    var nextVector = TransformCodec.DecodeNextVector(content);
                    content = nextVector.EncodedString;
                    v3rTransform.Position = nextVector.Vector;

                    //Rotation
                    var nextQuaternion = TransformCodec.DecodeNextQuaternion(content);
                    content = nextQuaternion.EncodedString;
                    v3rTransform.Rotation = nextQuaternion.Quaternion;

                    playerTransform.position = v3rTransform.Position;
                    playerTransform.rotation = v3rTransform.Rotation;
                }
            }
            
        });
    }

    #endregion
}