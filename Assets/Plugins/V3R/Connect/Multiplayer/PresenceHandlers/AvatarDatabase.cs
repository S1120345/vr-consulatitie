﻿using System;
using UnityEngine;

namespace V3R.Connect.Multiplayer
{
    [Serializable]
    [CreateAssetMenu(fileName = "Avatar Database", menuName = "V3R/Avatar Database")]
    public class AvatarDatabase : ScriptableObject
    {
        #region Variables

        public GameObject[] AvatarPrefabs;

        #endregion
    }
}
