﻿using System.Collections.Generic;
using Nakama;
using Plugins.V3R.Connect.Multiplayer.PresenceHandlers;
using UnityEngine;
using V3R.Avatar.Tracking;
using V3R.Common;
using V3R.Multiplayer;

namespace V3R.Connect.Multiplayer
{
    public class PlayerPresenceArmatureHandler : MonoBehaviour
    {
        #region Variables

        [SerializeField] public MatchSys _multiplayer;
        [SerializeField] public PlayerPresenceHandler _playerPresenceHandler;
        private string UserID;
        private float _time = 1f / 30f; //Update time, 30 Ticks per second

        #endregion

        #region Functions

        private void Start()
        {
            _multiplayer.AddStateListener(V3ROpCodes.ARMATURE, UpdatePlayerArmature);
            UserID = gameObject.GetComponent<SessionWithPreferences>().session.UserId;
            OnPlayerChanged();
            PlayerPresenceHandler.PlayerChanged += OnPlayerChanged;
        }


        private void OnPlayerChanged()
        {
            //TODO Change Player Model
        }


        private void UpdatePlayerArmature(IMatchState matchState)
        {
            if (matchState.UserPresence.UserId == UserID)
                if (_playerPresenceHandler.PlayerAvatars.TryGetValue(matchState.UserPresence.UserId,
                    out GameObject user))
                {
                    Destroy(user);
                }

            if (_playerPresenceHandler.PlayerAvatars.TryGetValue(matchState.UserPresence.UserId, out GameObject playerObject))
            {
                UnityMainThread.AddJob(() =>
                {
                    var content = matchState.State.FromUTF();
                    var armature = playerObject.GetComponent<Armature>();
                    
                    if (armature.GetTransforms() is null)
                        return;
                    
                    List<Transform> transforms = armature.GetTransforms();
                    foreach (var playerTransform in transforms)
                    {
                        var v3rTransform = new V3RTransform();
                
                        //Position
                        NextVector nextVector = TransformCodec.DecodeNextVector(content);
                        content = nextVector.EncodedString;
                        v3rTransform.Position = nextVector.Vector;

                        //Rotation
                        NextQuaternion nextQuaternion = TransformCodec.DecodeNextQuaternion(content);
                        content = nextQuaternion.EncodedString;
                        v3rTransform.Rotation = nextQuaternion.Quaternion;
                        
                        playerTransform.position = v3rTransform.Position;
                        playerTransform.rotation = v3rTransform.Rotation;
                    }
                    
                });
            }
            
            /*if (_playerPresenceHandler.PlayerAvatars.TryGetValue(matchState.UserPresence.UserId, out GameObject playerObject))
            {
                UnityMainThread.AddJob(() =>
                {
                    var content = matchState.State.FromUTF();

                    var armature = playerObject.GetComponent<Armature>();

                    List<BoneConfig> bones = armature.ArmatureConfig.Bones;
                    var transforms = new List<V3RTransform>();

                    for (var i = 0; i < bones.Count; i++)
                    {
                        var v3rTransform = new V3RTransform();

                        //Position
                        if (armature.ArmatureConfig.Bones[i].SyncPosition)
                        {
                            NextVector nextVector = TransformCodec.DecodeNextVector(content);
                            content = nextVector.EncodedString;
                            v3rTransform.Position = nextVector.Vector;
                        }

                        //Rotation
                        if (armature.ArmatureConfig.Bones[i].SyncRotation)
                        {
                            NextQuaternion nextQuaternion = TransformCodec.DecodeNextQuaternion(content);
                            content = nextQuaternion.EncodedString;
                            v3rTransform.Rotation = nextQuaternion.Quaternion;
                        }

                        transforms.Add(v3rTransform);
                    }

                    armature.PushTransforms(transforms);
                });
            }*/
        }

        #endregion
    }
}
