﻿using System.Collections.Generic;
using UnityEngine;

namespace Plugins.V3R.Connect.Multiplayer.PresenceHandlers
{
    public static class TransformCodec
    {
        #region Vector3

        public static string EncodeVector3(Vector3[] vectors)
        {
            string encodedString = "";

            foreach (var vector in vectors)
            {
                var x = ((int)(vector.x * 1000)).ToString();
                var y = ((int)(vector.y * 1000)).ToString();
                var z = ((int)(vector.z * 1000)).ToString();

                encodedString += x.Length;
                encodedString += x;

                encodedString += y.Length;
                encodedString += y;

                encodedString += z.Length;
                encodedString += z;
            }

            return encodedString;
        }


        public static List<Vector3> DecodeVector3(string encodedString)
        {
            var vectors = new List<Vector3>();

            while (encodedString.Length > 0)
            {
                var nextVector = DecodeNextVector(encodedString);
                encodedString = nextVector.EncodedString;
                vectors.Add(nextVector.Vector);
            }
            return vectors;
        }


        public static NextVector DecodeNextVector(string encodedString)
        {
            var nextVector = new NextVector();
            var numbers = new float[3];

            for (var i = 0; i < numbers.Length; i++)
            {
                char c = encodedString[0];
                var length = (int)char.GetNumericValue(c);
                string number = encodedString.Substring(1, length);
                numbers[i] = int.Parse(number) / 1000f;
                encodedString = encodedString.Substring(length + 1);
            }

            nextVector.EncodedString = encodedString;
            nextVector.Vector = new Vector3(numbers[0], numbers[1], numbers[2]);

            return nextVector;
        }

        #endregion

        #region Quaternion

        public static string EncodeQuaternion(Quaternion[] quaternions)
        {
            string encodedString = "";

            foreach (var quaternion in quaternions)
            {
                var x = ((int)(quaternion.x * 1000)).ToString();
                var y = ((int)(quaternion.y * 1000)).ToString();
                var z = ((int)(quaternion.z * 1000)).ToString();
                var w = ((int)(quaternion.w * 1000)).ToString();

                encodedString += x.Length + x;
                encodedString += y.Length + y;
                encodedString += z.Length + z;
                encodedString += w.Length + w;
            }

            return encodedString;
        }


        public static List<Quaternion> DecodeQuaternion(string encodedString)
        {
            var quaternions = new List<Quaternion>();

            while (encodedString.Length > 0)
            {
                var nextQuaternion = DecodeNextQuaternion(encodedString);
                encodedString = nextQuaternion.EncodedString;
                quaternions.Add(nextQuaternion.Quaternion);
            }
            return quaternions;
        }


        public static NextQuaternion DecodeNextQuaternion(string encodedString)
        {
            var nextQuaternion = new NextQuaternion();
            var numbers = new float[4];

            for (var i = 0; i < numbers.Length; i++)
            {
                char c = encodedString[0];
                var length = (int)char.GetNumericValue(c);
                string number = encodedString.Substring(1, length);
                numbers[i] = int.Parse(number) / 1000f;
                encodedString = encodedString.Substring(length + 1);
            }

            nextQuaternion.EncodedString = encodedString;
            nextQuaternion.Quaternion = new Quaternion(numbers[0], numbers[1], numbers[2], numbers[3]);

            return nextQuaternion;
        }

        #endregion

    }

    public struct NextVector
    {
        public string EncodedString;
        public Vector3 Vector;
    }

    public struct NextQuaternion
    {
        public string EncodedString;
        public Quaternion Quaternion;
    }

}
