﻿using System.Collections.Generic;
using Nakama;
using Plugins.V3R.Connect.Multiplayer.PresenceHandlers;
using UnityEngine;
using V3R.Common;
using V3R.Multiplayer;

namespace V3R.Connect.Multiplayer
{
    public class PlayerPresenceLookieHandler : MonoBehaviour
    {
        #region Variables

        [SerializeField] public MatchSys multiplayer;
        [SerializeField] public PlayerPresenceHandler playerPresenceHandler;
        
        [SerializeField] public Lookie Lookie;

        private float _time = 1f / 30f; //Update time, 30 Ticks per second
        private string UserID;
        #endregion

        #region Functions

        private void Start()
        {
            multiplayer.AddStateListener(V3ROpCodes.LOOKIE, UpdatePlayerLookie);
            UserID = gameObject.GetComponent<SessionWithPreferences>().session.UserId;
            OnPlayerChanged();
            PlayerPresenceHandler.PlayerChanged += OnPlayerChanged;
        }


        private void OnPlayerChanged()
        {
            //TODO Change Player Model
        }

        private void Update()
        {
            if (_time <= 0)
            {
                _time = 1f / 30f;

                if (Lookie is null)
                    return;
                
                var dataString = "";

                Transform[] transforms = Lookie.transforms;

                foreach (var transform in transforms)
                {
                    string nextVector =
                        TransformCodec.EncodeVector3(new[] { transform.position });
                    dataString += nextVector;

                    string nextQuaternion =
                        TransformCodec.EncodeQuaternion(new[] { transform.rotation });
                    dataString += nextQuaternion;
                }
                

                multiplayer.SendMatchState(V3ROpCodes.LOOKIE, dataString);
            }

            _time -= Time.deltaTime;
        }


        private void UpdatePlayerLookie(IMatchState matchState)
        {
            if (matchState.UserPresence.UserId == UserID)
                if (playerPresenceHandler.PlayerAvatars.TryGetValue(matchState.UserPresence.UserId,
                    out GameObject user))
                {
                    Destroy(user);
                }

            if (playerPresenceHandler.PlayerAvatars.TryGetValue(matchState.UserPresence.UserId, out GameObject playerObject))
            {
                UnityMainThread.AddJob(() =>
                {
                    var content = matchState.State.FromUTF();
                    var lookie = playerObject.GetComponent<Lookie>();

                    foreach (var playerTransform in lookie.transforms)
                    {
                        var v3rTransform = new V3RTransform();
                
                        //Position
                        NextVector nextVector = TransformCodec.DecodeNextVector(content);
                        content = nextVector.EncodedString;
                        v3rTransform.Position = nextVector.Vector;

                        //Rotation
                        NextQuaternion nextQuaternion = TransformCodec.DecodeNextQuaternion(content);
                        content = nextQuaternion.EncodedString;
                        v3rTransform.Rotation = nextQuaternion.Quaternion;
                        
                        playerTransform.position = v3rTransform.Position;
                        playerTransform.rotation = v3rTransform.Rotation;
                    }
                    
                });
            }
        }

        #endregion
    }
}
