﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nakama;
using UnityEngine;
using V3R.Avatar;
using V3R.Common;
using V3R.Multiplayer;

namespace V3R.Connect.Multiplayer
{
    public class PlayerPresenceHandler : MonoBehaviour
    {
        #region Variables

        [SerializeField] public MatchSys _multiplayer;
        [SerializeField] public AvatarDatabase _avatarDatabase;

        public readonly Dictionary<string, GameObject> PlayerAvatars = new Dictionary<string, GameObject>();

        #region Player

        public delegate void PlayerEvent();
        public static event PlayerEvent PlayerChanged;

        private static IPlayer _player;
        public static IPlayer Player
        {
            get => _player;
            set
            {
                _player = value;
                PlayerChanged?.Invoke();
            }
        }

        #endregion

        #endregion

        #region Functions
        void Start()
        {
            _multiplayer.AddStateListener(V3ROpCodes.PLAYER_INFO, InstantiateUser);
            _multiplayer.OnReceivedMatchPresence += UpdatePresences;
            _multiplayer.OnJoinMatch += JoinedMatch;
        }


        private void JoinedMatch(IMatch match)
        {
            var playerInfo = new PlayerInfo();
            playerInfo.PlayerModelID = 2;

            var json = JsonUtility.ToJson(playerInfo);
            _multiplayer.SendMatchState(V3ROpCodes.PLAYER_INFO, json);
        }


        private void UpdatePresences(IMatchPresenceEvent matchPresenceEvent)
        {
            if (matchPresenceEvent.Joins.Any())
            {
                var playerInfo = new PlayerInfo();
                playerInfo.PlayerModelID = 2;

                string json = JsonUtility.ToJson(playerInfo);
                _multiplayer.SendMatchState(V3ROpCodes.PLAYER_INFO, json);
            }

            foreach (IUserPresence disconnectingUser in matchPresenceEvent.Leaves)
            {
                UnityMainThread.AddJob(() =>
                {
                    Destroy(PlayerAvatars[disconnectingUser.UserId].gameObject);
                    PlayerAvatars.RemoveSafely(disconnectingUser.UserId);
                });
            }
        }


        private void InstantiateUser(IMatchState matchState)
        {
            Debug.Log($"Instantiating: {matchState.UserPresence.Username} Model!");

            UnityMainThread.AddJob(() =>
            {
                if (PlayerAvatars.ContainsKey(matchState.UserPresence.UserId) || _multiplayer.IsSelf(matchState.UserPresence.UserId))
                    return;

                Debug.Log($"Instantiating User: {matchState.UserPresence.Username}");

                string content = matchState.State.FromUTF();
                var info = JsonUtility.FromJson<PlayerInfo>(content);

                GameObject obj = Instantiate(_avatarDatabase.AvatarPrefabs[info.PlayerModelID]);
                PlayerAvatars.Add(matchState.UserPresence.UserId, obj);
            });
        }

        #endregion
    }


    [Serializable]
    public class PlayerInfo
    {
        public int PlayerModelID;
    }
}
