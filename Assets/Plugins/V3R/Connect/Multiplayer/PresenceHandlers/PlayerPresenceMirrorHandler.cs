﻿using System.Collections;
using System.Collections.Generic;
using Nakama;
using Plugins.V3R.Connect.Multiplayer.PresenceHandlers;
using UnityEngine;
using V3R.Common;
using V3R.Connect.Multiplayer;
using V3R.Multiplayer;
using V3R.Utility.Recording;

public class PlayerPresenceMirrorHandler : MonoBehaviour
{
    #region Variables

    [SerializeField] public MatchSys multiplayer;
    [SerializeField] public PlayerPresenceHandler playerPresenceHandler;
    [SerializeField] public GameObject Mirror;
    private float _time = 1f; //Update time, 1 Tick per second
    private string UserID;
    private bool oldMirror = false;

    #endregion

    #region Functions

    private void Start()
    {
        multiplayer.AddStateListener(V3ROpCodes.MIRROR, UpdatePlayerLookie);
        UserID = gameObject.GetComponent<SessionWithPreferences>().session.UserId;
    }


    private void Update()
    {
        if (_time <= 0)
        {
            _time = 1f;

            if (Mirror is null)
                return;

            var dataString = "";
            if (oldMirror == Mirror.activeInHierarchy)
                return;
            
            if (Mirror.activeInHierarchy)
                dataString += 1;
            else
                dataString += 0;

            oldMirror = Mirror.activeInHierarchy;

            multiplayer.SendMatchState(V3ROpCodes.MIRROR, dataString);
        }

        _time -= Time.deltaTime;
    }


    private void UpdatePlayerLookie(IMatchState matchState)
    {
        if (matchState.UserPresence.UserId == UserID)
            return;
        UnityMainThread.AddJob(() =>
        {
            var content = matchState.State.FromUTF();

            if (content.StartsWith("0"))
            {
                Mirror.SetActive(false);
                return;
            }
            Mirror.SetActive(true);
        });
    }
    #endregion
}
