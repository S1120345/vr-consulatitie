﻿using System;
using System.Collections.Generic;
using Nakama;
using Nakama.TinyJson;
using UnityEngine;
using V3R.Common;

//https://heroiclabs.com/docs/runtime-code-basics/#rpc-example
//
namespace V3R.Multiplayer
{
    public class MatchSys : MonoBehaviour
    {
        //variables
        [Tooltip("Create match automatically")] [SerializeField]
        public bool debugAutoCreate;

        [Tooltip("Join match on load, only for auto-created matches")] [SerializeField]
        public bool debugAutoJoin;

        [Tooltip("Join Existing match")] [SerializeField]
        public bool joinExistingMatch;

        [Tooltip("Match id \nExample: 58d2fe0b-8b5e-4125-905b-d980dfad582e.dev_server")] [SerializeField]
        public string matchId;

        private string _mId;
        private IMatch _match;
        private Dictionary<long, Action<IMatchState>> _stateDictionary = new Dictionary<long, Action<IMatchState>>();

        //create match
        async void MatchCreate()
        {
            //find client & session
            IClient client = gameObject.GetComponent<SessionWithPreferences>().client;
            ISession session = gameObject.GetComponent<SessionWithPreferences>().session;

            //create match
            var payload = "{}";
            IApiRpc CreatedMatch = await client.RpcAsync(session, "create_match_rpc", payload);
            _mId = CreatedMatch.Payload;
            Debug.LogFormat("created: " + _mId);

            //if selected, join created match
            if (debugAutoJoin)
            {
                MatchJoin(_mId);
            }
        }

        //async join match
        //fill mId in manually
        async void MatchJoin(string mId)
        {
            //show match id
            Debug.LogFormat("joining: " + mId);
            _mId = mId;

            //find socket
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            //join match
            _match = await socket.JoinMatchAsync(mId);
            OnJoinMatch?.Invoke(_match);

            //list users connected so far
            foreach (var presence in _match.Presences)
            {
                Debug.LogFormat("User id: '{0}', name: '{1}'.", presence.UserId, presence.Username);
            }
        }

        /// <summary>
        /// code for armature and presence handlers
        /// </summary>
        ///

        public Action<IMatchPresenceEvent> OnReceivedMatchPresence;

        public Action<IMatch> OnJoinMatch;


        public bool IsSelf(string userId)
        {
            return userId == GetComponent<SessionWithPreferences>().session.UserId;
        }
            

        //find socket
        public void AddStateListener(int opCode, Action<IMatchState> callback)
        {
            _stateDictionary.AddOrUpdate(opCode, callback);
        }

        public async void SendMatchState(int opCode, string state)
        {
            ISocket _socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            if (_socket != null)
                await _socket.SendMatchStateAsync(_mId, opCode, state);
        }

        // Start is called before the first frame update
        void Start()
        {
            //create match
            if (debugAutoCreate)
            {
                MatchCreate();
            }

            //if manually join existing match
            if (joinExistingMatch)
            {
                MatchJoin(matchId);
            }

            //find socket
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            socket.ReceivedMatchState += matchState =>
            {
                if (_stateDictionary.TryGetValue(matchState.OpCode, out Action<IMatchState> callback))
                {
                    callback?.Invoke(matchState);
                }

            };

            socket.ReceivedMatchPresence += matchPresence => OnReceivedMatchPresence?.Invoke(matchPresence);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
