using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Nakama;
using Nakama.TinyJson;
using UnityEngine;
using UnityEngine.Serialization;

namespace V3R.Multiplayer
{
    public class ChatChannel : MonoBehaviour
    {
        /* Channel types:
         * ZONE/ROOM = every one, room
         * GROUP = only group members
         * DIRECT = only between two people, ONLY if both online
         */

        #region Variables

        //Unity editable variables
        private enum ChannelType
        {
            ZONE,
            GROUP,
            DIRECT
        };

        [FormerlySerializedAs("_channelTypeEnum")] [Tooltip("Choose the type of chat _channel")] [SerializeField]
        ChannelType channelTypeEnum;

        [FormerlySerializedAs("_channelName")] [Tooltip("Channel name")] [SerializeField]
        public string channelName;

        [Tooltip("Toggles if the Channel is hidden or not")] [SerializeField]
        private bool _hidden;

        [Tooltip("Toggles if chat messages will stay forever or not")] [SerializeField]
        private bool _persistence = true;

        [Tooltip(
            "Toggles debug settings on/off\nIncludes:\nTest message\nGroup name input instead of id input\nUsername input instead of id input\nNOTE: performance cost, use for debug only")]
        [SerializeField]
        private bool _Debug;

        //Channel variables
        private int _messageCounter = 0;
        private IChannel _channel;
        private string _channelId;
        private string _groupId;
        private string _userId;
        private List<Dictionary<string, string>> _messages = new List<Dictionary<string, string>>();

        #endregion


        #region Functions

        async void Start()
        {
            //parse socket & _channel
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            //join selected _channel
            switch (channelTypeEnum)
            {
                case ChannelType.ZONE:
                    _channel = await socket.JoinChatAsync(channelName, Nakama.ChannelType.Room, _persistence, _hidden);
                    break;
                case ChannelType.GROUP:
                    if (_Debug)
                    {
                        GetUserGroupIdByName();
                    }
                    else _groupId = channelName;

                    _channel = await socket.JoinChatAsync(_groupId, Nakama.ChannelType.Group, _persistence, _hidden);
                    break;
                case ChannelType.DIRECT:
                    if (_Debug)
                    {
                    }
                    else _userId = channelName;

                    _channel = await socket.JoinChatAsync(_userId, Nakama.ChannelType.DirectMessage, _persistence,
                        _hidden);
                    break;
            }

            _channelId = _channel.Id;

            Debug.LogFormat("You can now send messages to _channel id: '{0}'", _channelId);

            //turn on message receiving
            socket.ReceivedChannelMessage += message =>
            {
                if (message.ChannelId == _channelId)
                {
                    _messages.Add(new Dictionary<string, string>()
                    {
                        {"type", message.Code.ToString()}, {"senderName", message.Username},
                        {"content", message.Content}
                    });
                }
            };

            //debug if selected
            if (_Debug)
            {
                SendMessage("Hello World!");
                ReceiveMessageDebug();
            }

        }

        private void Update()
        {

        }

        /// <summary>
        /// leave chat
        /// note that this doesn't remove the chatroom, much like real live rooms don't disappear if you leave them. Not that would be something...
        /// </summary>
        public async void LeaveChannel()
        {
            //parse socket & _channel
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            //do function
            await socket.LeaveChatAsync(_channelId);

            //remove ChatChannel from GameObject
        }

        /// <summary>
        /// return all online users in _channel
        /// </summary>
        public void ListOnline()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// send message
        /// </summary>
        public async void SendMessage(string message)
        {
            //parse socket & _channel
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            //do function
            _messageCounter++;
            string key = channelName + _messageCounter;
            string content = new Dictionary<string, string> {{key, message}}.ToJson();
            string sendId = "";
            switch (channelTypeEnum)
            {
                case ChannelType.ZONE:
                    sendId = _channelId;
                    break;
                case ChannelType.GROUP:
                    sendId = _groupId;
                    break;
                case ChannelType.DIRECT:
                    sendId = _userId;
                    break;
            }

            var sendAck = await socket.WriteChatMessageAsync(sendId, content);
            Debug.LogFormat("send: {0}", sendAck);
        }

        /// <summary>
        /// Returns all messages received from channel
        /// </summary>
        /// <returns>Received Message Content</returns>
        public List<Dictionary<string, string>> ReturnMessages()
        {
            return _messages;
        }

        /// <summary>
        /// return received message info to debug log
        /// messages can be drawn onto UI trough socket object, no need for function.
        /// for types, check nakama documentation: https://heroiclabs.com/docs/social-realtime-chat/
        /// </summary>
        private void ReceiveMessageDebug()
        {
            //parse socket
            ISocket socket = gameObject.GetComponent<SessionWithPreferences>().socket.socket;

            //do function
            socket.ReceivedChannelMessage += message =>
            {
                Debug.LogFormat("Received: {0}", message);
                Debug.LogFormat("Message has _channel id: {0}", message.ChannelId);
                Debug.LogFormat("Message has type: {0}", message.Code);
                Debug.LogFormat("Message has content {0}", message.Content);
            };
        }

        /// <summary>
        /// returns userIds based on a username (exact match)
        /// due to checking all users, only for debug mode for easy testing in Unity interface
        /// proper implementation should just enter the Id as channel name
        /// </summary>
        private async void GetUserGroupIdByName()
        {
            IClient client = gameObject.GetComponent<SessionWithPreferences>().client;
            ISession session = gameObject.GetComponent<SessionWithPreferences>().session;
            var result = await client.ListUserGroupsAsync(session, session.UserId);
            foreach (var g in result.UserGroups)
            {
                if (g.Group.Name == channelName)
                {
                    _groupId = g.Group.Id;
                }
            }
        }

        #endregion

    }
}