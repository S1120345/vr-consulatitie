﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Nakama;
using UnityEngine;
using V3R.Multiplayer;
using Random = System.Random;

namespace V3R.Multiplayer
{
    public class SessionWithPreferences : MonoBehaviour
    {
        #region Variables

        //Unity editable variables
        private enum ServerEnum
        {
            MANUAL,
            LOCAL,
            VLLOCALTEST,
            TEST,
            PRODUCTION
        };

        [Tooltip("choose what server to connect to.")] [SerializeField]
        private ServerEnum _server;

        //possible clients to run
        private IClient _localClient = new Client("http", "127.0.0.1", 7350, "defaultkey");
        private IClient _VLlocalTestClient = new Client("http", "172.25.25.60", 7350, "defaultkey");
        private IClient _testClient = new Client("http", "172.25.141.24", 7350, "defaultkey");
        private IClient _productionClient = new Client("http", "172.25.140.24", 7350, "defaultkey");

        //manual client info
        [Tooltip("default: http")] public string manualScheme = "http";
        [Tooltip("default: 127.0.0.1")] public string manualHost = "127.0.0.1";
        [Tooltip("default: 7350")] public int manualPort = 7350;
        [Tooltip("default: defaultkey")] public string manualServerKey = "defaultkey";
        public bool manualWebplayer = false;

        [NonSerialized]public bool done = false;
        //used/choosen client
        public IClient client;

        //general/other variables
        private const string PREFERRED_KEY_NAME = "nakama.session";
        public ISession session;
        public RealtimeSocketConnection socket;

        #endregion


        #region Functions

        private async void Awake()
        {
            //disable components if enabled to ensure things boot in the right order
            if (gameObject.TryGetComponent(out ChatChannel ccf) == true)
            {
                gameObject.GetComponent<ChatChannel>().enabled = false;
            }

            if (gameObject.TryGetComponent(out MatchSys ms) == true)
            {
                gameObject.GetComponent<MatchSys>().enabled = false;
            }

            //check for existing session. If so use that, else continue.
            //start authenticating
            var authenticationToken = PlayerPrefs.GetString(PREFERRED_KEY_NAME);
            if (string.IsNullOrEmpty(authenticationToken) == false)
            {
                var session = Session.Restore(authenticationToken);
                if (session.IsExpired == false)
                {
                    this.session = session;
                    Debug.Log(this.session);
                    return;
                }
            }

            /* authenticate server access trough email/password login
            const string email = "";
            const string password = "";
            var _session = await client.AuthenticateEmailAsync(email, password);
            Debug.Log(_session);
            */
            //authenticate server access trough device
            
            var deviceId = PlayerPrefs.GetString("nakama.deviceid");
            if (deviceId.Equals("n/a")|| string.IsNullOrEmpty(deviceId))
            {
                Debug.LogWarning("systeminfo");
                Debug.Log($"deviceID: {SystemInfo.deviceUniqueIdentifier}");
                deviceId = GenerateRandomAlphanumericString(15);
                PlayerPrefs.SetString("nakama.deviceid", deviceId);
            }

            //select channel to chosen one. Not in set in Start to avoid 
            switch (_server)
            {
                case ServerEnum.MANUAL:
                    if (manualWebplayer)
                    {
                        client = new Client(manualScheme, manualHost, manualPort, manualServerKey,
                            UnityWebRequestAdapter.Instance);
                    }
                    else
                    {
                        client = new Client(manualScheme, manualHost, manualPort, manualServerKey);
                    }

                    break;
                case ServerEnum.LOCAL:
                    client = _localClient;
                    break;
                case ServerEnum.VLLOCALTEST:
                    client = _VLlocalTestClient;
                    break;
                case ServerEnum.TEST:
                    client = _testClient;
                    break;
                case ServerEnum.PRODUCTION:
                    client = _productionClient;
                    break;
            }

            //authenticate
            //only auth when known deviceid
            session = await client.AuthenticateDeviceAsync(deviceId);
            Debug.LogFormat("New user: {0}, {1}", session.Created, session);
            
            //show debug info about session
            Debug.Log(session.AuthToken); //raw JWT token
            Debug.LogFormat("Session user id: '{0}'", session.UserId);
            Debug.LogFormat("Session user username: '{0}'", session.Username);
            Debug.LogFormat("Session has expired: {0}", session.IsExpired);
            Debug.LogFormat("Session expires at: {0}", session.ExpireTime);

            //create and connect to a realtime socket connection
            socket = new RealtimeSocketConnection(client, session);
            socket.socket.Connected += SetupMultiplayer;
            //activate chats if existing
            // if (gameObject.TryGetComponent(out ChatChannel cct))
            // {
            //     socket.socket.Connected += EnableChats;
            // }
            //
            // //activate matchsys if existing
            // if (gameObject.TryGetComponent(out MatchSys msys))
            // {
            //     socket.socket.Connected += EnableMatchsys;
            // }


        }

        public static string GenerateRandomAlphanumericString(int length = 10)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
 
            var random       = new Random();
            var randomString = new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            return randomString;
        }
        
        private void SetupMultiplayer()
        {
            done = true;
        }
        // /// <summary>
        // /// function to enable chat channel;
        // /// </summary>
        // private void EnableChats()
        // {
        //     gameObject.GetComponent<ChatChannel>().enabled = true;
        // }
        //
        // /// <summary>
        // /// function to enable matchsys;
        // /// </summary>
        // private void EnableMatchsys()
        // {
        //     gameObject.GetComponent<MatchSys>().enabled = true;
        // }





        #endregion
    }
}